# Build the QTLSearch database to create qtlsearch.db
qtlsearch-init --species ARATH --qtl qtls.tsv --annotation_map mapping.tsv

# Run the search (without empirical p estimation)
qtlsearch-run --species ARATH --qtl qtls.tsv --db qtlsearch.db
